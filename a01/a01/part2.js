function p2() {

  const fractions = require('./fractions');

  let act; // the actual value
  let type; // test type

  function assert(exp, act, type) {
    console.assert(exp.num === act.num && exp.denom === act.denom,
      't=%s exp=%o act=%o', type, exp, act);
  }

  console.log("Part 2 begin");

  // add
  type = 'add number';
  act = fractions.add(1, {num:1,denom:2});
  assert({ num: 3, denom: 2 }, act, type);
  act = fractions.add({num:1,denom:2}, 1);
  assert({ num: 3, denom: 2 }, act, type);
  act = fractions.add(1, 2);
  assert({ num: 3, denom: 1 }, act, type);

  // subtract
  type = 'sub number';
  act = fractions.subtract(1, {num:1,denom:4});
  assert({num:3,denom:4}, act, type);
  act = fractions.subtract({num:1,denom:4}, 1);
  assert({num:-3,denom:4}, act, type);
  act = fractions.subtract(1, 2);
  assert({num:-1,denom:1}, act, type);

  // mult
  type = 'mult number';
  act = fractions.multiply(2, {num:3, denom:4});
  assert({num:3,denom:2}, act, type);
  act = fractions.multiply({num:3, denom:4}, -2);
  assert({num:-3,denom:2}, act, type);
  act = fractions.multiply(-2, 4);
  assert({num:-8,denom:1}, act, type);

  // div
  type = 'div number';
  act = fractions.divide(6, {num:3, denom:4});
  assert({num:8,denom:1}, act, type);
  act = fractions.divide({num:3, denom:4}, -2);
  assert({num:-3,denom:8}, act, type);
  act = fractions.divide(-2, 4);
  assert({num:-1,denom:2}, act, type);

  // inverse
  type = 'inverse number';
  act = fractions.inverse(3);
  assert({num:1,denom:3}, act, type);
  act = fractions.inverse(-5);
  assert({num:-1,denom:5}, act, type);

  // equals
  type = 'equals number';
  console.assert(fractions.equals(2, {num:2, denom:1}));
  console.assert(fractions.equals({num:25, denom:5}, 5));
  console.assert(fractions.equals(3, {num:-9, denom:-3}));
  console.assert(fractions.equals(4, 4));
  console.assert(!fractions.equals(2, {num:1, denom:2}));
  console.assert(!fractions.equals({num:1, denom:3}, 3));
  console.assert(!fractions.equals(2, 4));

  console.log("Part 2 end");
}

module.exports = p2;
